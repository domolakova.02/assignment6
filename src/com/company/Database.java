package com.company;

import java.sql.*;

public class Database {
    private static Database instance;
    String connectionUrl = "jdbc:postgresql://localhost:5432/Assignment6";
    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;

    private Database(){
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(connectionUrl, "postgres", "9696");

        } catch (Exception e){
            System.out.println(e);
        }
    }

    public static Database getInstance(){
        if(instance == null){
            instance = new Database();
        }
        return instance;
    }

    public void insertData() {
        try {
            String query = "insert into Database values (4, 'Jake', 'Pitter', '12/05/1995')";
            statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e){
            System.out.println(e);
        }
    }

    public void readData(){
        try {
            String query1 = "select * from Database";
            statement = connection.createStatement();
            statement.executeUpdate(query1);
            System.out.println(statement);
            String id ;
        } catch (Exception e){
            System.out.println(e);
        }
    }
}
