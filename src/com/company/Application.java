package com.company;

public class Application {

    public static void main(String[] args) {
        Database foo = Database.getInstance();
       foo.insertData();
       foo.readData();

        Database bar = Database.getInstance();
        bar.insertData();

    }
}
