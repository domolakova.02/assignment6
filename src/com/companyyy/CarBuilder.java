package com.companyyy;

public class CarBuilder implements Builder{
    private Car car;
    private CarType type;
    private int seats;
    private Engine engine;
    private TripComputer tripComputer;
    private GPS gps;

    public void setCarType(CarType type) {
        this.type = type;
    }

    public void reset(){
        this.car = new Car();
    }

    @Override
    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }


    @Override
    public void setTripComputer(TripComputer tripComputer) {
        this.tripComputer = tripComputer;
    }

    @Override
    public void setGPS(GPS gps) {
        this.gps= gps;
    }

    public Car getProduct() {
        Car product = this.car;
        this.reset();
        return product;
    }
}
