package com.companyyy;

public class CarManualBuilder implements Builder{
    private CarType type;
    private int seats;
    private Engine engine;
    private TripComputer tripComputer;
    private GPS gps;
    Manual manual;

    public CarManualBuilder (){
        this.reset();
    }

    public void reset(){
        this.manual = new Manual();
    }

    @Override
    public void setCarType(CarType sport_car) {

    }

    @Override
    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void setTripComputer(TripComputer tripComputer) {
        this.tripComputer = tripComputer;
    }

    @Override
    public void setGPS(GPS gps) {
        this.gps = gps;
    }

    public Manual getProduct() {
        Manual product = this.manual;
        this.reset();
        return product;
    }
}
