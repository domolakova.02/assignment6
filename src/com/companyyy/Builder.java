package com.companyyy;

public interface Builder {
    void setCarType(CarType sport_car);
    void setSeats(int seats);
    void setEngine(Engine engine);
    void setTripComputer(TripComputer tripComputer);
    void setGPS(GPS gps);
}
