package com.companyyy;

public class Director {
    Builder builder;

    public void setBuilder(Builder builder){
        this.builder=builder;
    }

    public void sportCar(Builder builder){
        builder.setCarType(CarType.Sport_car);
        builder.setSeats(2);
        builder.setEngine(new Engine(3.0, 0));
        builder.setTripComputer(new TripComputer());
        builder.setGPS(new GPS());
    }

    public void SUV(Builder builder) {
        builder.setCarType(CarType.SUV);
        builder.setSeats(4);
        builder.setEngine(new Engine(2.5, 0));
        builder.setGPS(new GPS());
    }

    public void cabriolet(Builder builder) {
        builder.setCarType(CarType.Cabriolet);
        builder.setSeats(4);
        builder.setEngine(new Engine(2.0, 0));
        builder.setGPS(new GPS());
    }
}
