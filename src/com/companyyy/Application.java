package com.companyyy;

public class Application {
    public static void main(String[] args){
    Director director = new Director();
    CarBuilder builder = new CarBuilder();
    director.sportCar(builder);
    Car car = builder.getProduct();
    System.out.println("Car is :" + car.getType_of_car());

    CarManualBuilder carManualBuilder = new CarManualBuilder();
    director.sportCar(carManualBuilder);

    Manual carManual = carManualBuilder.getProduct();
    System.out.println(carManual.print());
    }
}
