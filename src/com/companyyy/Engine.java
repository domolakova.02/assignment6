package com.companyyy;

public class Engine {
    private double volume;
    private double length;
    private boolean start;

    public Engine(double v, double l){
        this.volume = v;
        this.length = l;
    }

    public double getLength(){
        return length;
    }

    public double getVolume() {
        return volume;
    }

    public void connected(){
        start = true;
    }

    public void notConnected(){
        start = false;
    }

    public boolean isStart(){
        return start;
    }

    public void go(double l){
        if (start){
            this.length +=l;
        } else {
            System.out.println("Error");
        }
    }
}
