package com.companyyy;

public class Car {
    private CarType type_of_car;
    private int seats;
    private Engine engine;
    private GPS gps;
    private TripComputer tripComputer;

    public Car(CarType type_of_car, int seats, Engine engine, TripComputer tripComputer, GPS gps){
        this.type_of_car =type_of_car;
        this.seats = seats;
        this.engine = engine;
        this.gps = gps;
        this.tripComputer = tripComputer;
    }

    public Car() {

    }

    public CarType getType_of_car(){
        return type_of_car;
    }

    public int getSeats() {
        return seats;
    }

    public Engine getEngine() {
        return engine;
    }

    public GPS getGps() {
        return gps;
    }

    public TripComputer getTripComputer() {
        return tripComputer;
    }
}

