package com.companyyy;

public class TripComputer {
    private Car car;
    public void setCar(Car car){
        this.car = car;
    }

    public void status(){
        if (this.car.getIngine().status()){
            System.out.println("Started");
        } else {
            System.out.println("Not Started");
        }
    }
}
