package com.companyy;

public abstract class Dialog {
    abstract Button createButton();
    void render(){
        Button okButton = createButton();
        okButton.onClick("closeDialog");
        okButton.render();
    }
}
