package com.companyy;

public class Application {
    Dialog dialog;
    String config = "Web";

    void initialize() throws Exception{
        if (config.equals("Windows")){
            dialog = new WindowsDialog();
        } else if(config.equals("Web")){
            dialog = new WebDialog();
        } else throw new Exception("Error! Unknown operating system.");
    }
    public void main() throws Exception{
        initialize();
        dialog.render();
    }
}
