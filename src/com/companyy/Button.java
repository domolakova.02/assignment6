package com.companyy;

public interface Button {
    void render();
    void onClick(String f);
}
